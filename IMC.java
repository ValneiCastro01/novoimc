
package imc;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class IMC {
	
	public static void main(String[] args) { // faz acontecer 
		ArrayList<Pessoa> valores = new arquivo().loader("C:\\\\Users\\\\Valnei Castro\\\\Downloads\\dataset.csv");
		try(FileWriter fileWriter = new FileWriter("ValneiAlbuquerqueDeCastroFilho.txt")){
			for(Pessoa  n : valores) {
				double soma;
				soma = n.getPeso() / (n.getAltura() * n.getAltura());
				String resultado = String.format("%.2f", soma); //formatou as casas depois do . em ate duas casas decimais.
				fileWriter.write(n.getNome().toUpperCase() + " " + n.getSobrenome().toUpperCase() +  " "  +  resultado + "\n");
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}

}
